<?php
/**
* Created by PhpStorm.
* User: ralf
* Date: 03.09.15
* Time: 20:22
*/
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$connection = $installer->getConnection();

// table names
$reminderTableName = 'mageconsult_shopvote/reminder';

// drop table if exists
$reminderTable = $connection->dropTable($installer->getTable($reminderTableName));

// reminder table
$reminderTable = $connection->newTable($installer->getTable($reminderTableName));

$reminderTable
    ->addColumn('reminder_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'identity'  => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
        'auto_increment' => true
    ))
    ->addColumn('invoice_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable' => false,
        'unsigned' => true,
    ))
    ->addColumn('increment_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable' => false,
        'unsigned' => true,
    ))
    ->addColumn('email', Varien_Db_Ddl_Table::TYPE_VARCHAR, 80, array(
        'nullable' => false,
    ))
    ->addColumn('customer_firstname', Varien_Db_Ddl_Table::TYPE_VARCHAR, 80, array(
        'nullable' => false,
    ))
    ->addColumn('customer_lastname', Varien_Db_Ddl_Table::TYPE_VARCHAR, 80, array(
        'nullable' => false,
    ))
    ->addColumn('comment', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(),'comment, if added manually')
    ->addColumn('items_ordered', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false,
    ))
    ->addColumn('ordered_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))
    ->addColumn('processed_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => true,
        'default' => null
    ))
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_INTEGER, 2, array(
        'nullable' => false,
        'unsigned' => true,
    ))
    ->addColumn('added_manually', Varien_Db_Ddl_Table::TYPE_INTEGER, 1, array(
        'nullable' => false,
        'unsigned' => true,
        'default' => false
    ))
    ->addIndex(
        $installer->getIdxName(
            $installer->getTable($reminderTableName),
            array('invoice_id'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('invoice_id')
    )
    ->addIndex(
        $installer->getIdxName(
            $installer->getTable($reminderTableName),
            array('ordered_at'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('ordered_at')
    );
try {
    $connection->createTable($reminderTable);
} catch (Exception $e) {
    Mage::logException($e);
}

$installer->endSetup();