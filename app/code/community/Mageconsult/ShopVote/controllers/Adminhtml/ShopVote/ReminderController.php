<?php

/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 03.09.15
 * Time: 20:28
 */
class Mageconsult_ShopVote_Adminhtml_ShopVote_ReminderController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('mageconsult_shopvote/adminhtml_reminder'));
        $this->renderLayout();
    }

    public function exportCsvAction()
    {
        $fileName = 'Shopvote Reminders_export.csv';
        $content  = $this->getLayout()->createBlock('mageconsult_shopvote/adminhtml_reminder_grid')->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportExcelAction()
    {
        $fileName = 'Shopvote Reminders_export.xml';
        $content  = $this->getLayout()->createBlock('mageconsult_shopvote/adminhtml_reminder_grid')->getExcel();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function massDeleteAction()
    {
        $ids = $this->getRequest()->getParam('ids');
        if (!is_array($ids)) {
            $this->_getSession()->addError($this->__('Please select Shopvote Reminders(s).'));
        } else {
            try {
                foreach ($ids as $id) {
                    $model = Mage::getSingleton('mageconsult_shopvote/reminder')->load($id);
                    $model->delete();
                }

                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) have been deleted.', count($ids))
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('mageconsult_shopvote')->__('An error occurred while mass deleting items. Please review log and try again.')
                );
                Mage::logException($e);
                return;
            }
        }
        $this->_redirect('*/*/index');
    }


    /**
     * scan manually for new reminders
     */
    public function scanAction()
    {

        /** @var $reminderModel Mageconsult_ShopVote_Model_Reminder */
        $reminderModel = Mage::getModel('mageconsult_shopvote/reminder');
        $reminderModel->checkForNewReminders();

        $this->_redirect('*/*/index');

    }

    /**
     * send email to one customer
     * @throws Exception
     * @throws Mage_Core_Exception
     */
    public function emailAction()
    {

        $reminderId = $this->getRequest()->getParam('reminder_id');

        /** @var $reminder Mageconsult_ShopVote_Model_Reminder */
        $reminder = Mage::getModel('mageconsult_shopvote/reminder')->load($reminderId);

        if ($reminder->getId()) {

            $customerName  = trim($reminder->getData('customer_firstname')) . ' ' . trim($reminder->getData('customer_lastname'));
            $customerEmail = $reminder->getData('email');

            /** @var $order Mage_Sales_Model_Order */
            $order = Mage::getModel('sales/order')->load($reminder->getData('invoice_id'));

            $shipment = $order->getShipment();

            $emailTemplate = Mage::getModel('core/email_template')->loadDefault('mageconsult_shopvote_email_reminder');

            $storeId = $order->getStoreId();

            //Variables for Confirmation Mail.
            $emailTemplateVariables             = array();
            $emailTemplateVariables['name']     = $customerName;
            $emailTemplateVariables['email']    = $customerEmail;
            $emailTemplateVariables['order']    = $order;
            $emailTemplateVariables['shipment'] = $shipment;
            $emailTemplateVariables['store']    = Mage::getModel('core/store')->load($storeId);

            /** @var $email Mage_Core_Model_Email_Template */
            $email = Mage::getModel('core/email_template');

            $email->setDesignConfig(array('area' => Mage_Core_Model_App_Area::AREA_FRONTEND, 'store' => $storeId));
            $email->addBcc(Mage::getStoreConfig('trans_email/ident_general/email'));

            // get customer name
            if ($order->getCustomerIsGuest()) {
                $customerName = $order->getBillingAddress()->getName();
            } else {
                $customerName = $order->getCustomerName();
            }

            // send the mail
            $email->sendTransactional(
                'mageconsult_shopvote_email_reminder',
                'sales',
                $order->getCustomerEmail(),
                $customerName,
                $emailTemplateVariables,
                $storeId
            );

            // check for success
            if (!$email->getSentSuccess()) {
                Mage::throwException('Could not send e-mail with gift card for item ' . $reminder->getId());
            }

            $reminder->setStatus(Mageconsult_ShopVote_Model_Reminder::REMINDER_STATUS_SENT);
            $reminder->setProcessedAt(now());
            $reminder->save();

        }

        $this->_redirect('*/*/index');

    }

    /**
     * edit reminder
     */
    public function editAction()
    {
        $id    = $this->getRequest()->getParam('id');
        $model = Mage::getModel('mageconsult_shopvote/reminder');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->_getSession()->addError(
                    Mage::helper('mageconsult_shopvote')->__('This Shopvote Reminders no longer exists.')
                );
                $this->_redirect('*/*/');
                return;
            }
        }

        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('reminder_model', $model);

        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('mageconsult_shopvote/adminhtml_reminder_edit'));
        $this->renderLayout();
    }


    public function newAction()
    {
        $this->_forward('edit');
    }

    public function saveAction()
    {
        $redirectBack = $this->getRequest()->getParam('back', false);
        if ($data = $this->getRequest()->getPost()) {

            $id    = $this->getRequest()->getParam('id');
            $model = Mage::getModel('mageconsult_shopvote/reminder');
            if ($id) {
                $model->load($id);
                if (!$model->getId()) {
                    $this->_getSession()->addError(
                        Mage::helper('mageconsult_shopvote')->__('This Shopvote Reminders no longer exists.')
                    );
                    $this->_redirect('*/*/index');
                    return;
                }
            }

            // save model
            try {
                $model->addData($data);
                $this->_getSession()->setFormData($data);
                $model->save();
                $this->_getSession()->setFormData(false);
                $this->_getSession()->addSuccess(
                    Mage::helper('mageconsult_shopvote')->__('The Shopvote Reminders has been saved.')
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $redirectBack = true;
            } catch (Exception $e) {
                $this->_getSession()->addError(Mage::helper('mageconsult_shopvote')->__('Unable to save the Shopvote Reminders.'));
                $redirectBack = true;
                Mage::logException($e);
            }

            if ($redirectBack) {
                $this->_redirect('*/*/edit', array('id' => $model->getId()));
                return;
            }
        }
        $this->_redirect('*/*/index');
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                // init model and delete
                $model = Mage::getModel('mageconsult_shopvote/reminder');
                $model->load($id);
                if (!$model->getId()) {
                    Mage::throwException(Mage::helper('mageconsult_shopvote')->__('Unable to find a Shopvote Reminders to delete.'));
                }
                $model->delete();
                // display success message
                $this->_getSession()->addSuccess(
                    Mage::helper('mageconsult_shopvote')->__('The Shopvote Reminders has been deleted.')
                );
                // go to grid
                $this->_redirect('*/*/index');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('mageconsult_shopvote')->__('An error occurred while deleting Shopvote Reminders data. Please review log and try again.')
                );
                Mage::logException($e);
            }
            // redirect to edit form
            $this->_redirect('*/*/edit', array('id' => $id));
            return;
        }
        // display error message
        $this->_getSession()->addError(
            Mage::helper('mageconsult_shopvote')->__('Unable to find a Shopvote Reminders to delete.')
        );
        // go to grid
        $this->_redirect('*/*/index');
    }


    /**
     * ignore this order
     */
    public function ignoreAction()
    {
        $id = $this->getRequest()->getParam('id');

        try {
            /** @var $reminder Mageconsult_ShopVote_Model_Reminder */
            $reminder = Mage::getModel('mageconsult_shopvote/reminder');

            $reminder->load($id);
            if ($reminder->getId()) {
                if ((int) $reminder->getStatus() === Mageconsult_ShopVote_Model_Reminder::REMINDER_STATUS_IGNORE) {
                    $reminder->setStatus(Mageconsult_ShopVote_Model_Reminder::REMINDER_STATUS_NEW);
                } else
                {
                    $reminder->setStatus(Mageconsult_ShopVote_Model_Reminder::REMINDER_STATUS_IGNORE);
                }
                $reminder->setData('processed_at', now());
                $reminder->save();
            }
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addError(
                Mage::helper('mageconsult_shopvote')->__('An error occurred while ignoring Shopvote Reminders data. Please review log and try again.')
            );
            Mage::logException($e);
        }

        $this->_redirect('*/*/index');

    }


    /**
     * Initialize order model instance
     *
     * @return Mage_Sales_Model_Order || false
     */
    protected function _initOrder()
    {
        $id    = $this->getRequest()->getParam('order_id');
        $order = Mage::getModel('sales/order')->load($id);

        if (!$order->getId()) {
            $this->_getSession()->addError($this->__('This order no longer exists.'));
            $this->_redirect('*/*/');
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
            return false;
        }
        Mage::register('sales_order', $order);
        Mage::register('current_order', $order);
        return $order;
    }

    /**
     * ACL checking
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('shopvote/reminder');
    }
}