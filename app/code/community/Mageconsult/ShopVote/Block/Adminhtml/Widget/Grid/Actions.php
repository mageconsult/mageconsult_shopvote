<?php

class Mageconsult_ShopVote_Block_Adminhtml_Widget_Grid_Actions extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * Renders grid column
     *
     * @param   Varien_Object $row
     * @return  string
     */
    public function render(Varien_Object $row)
    {

        $return = '';

        if ($row->getStatus() == Mageconsult_ShopVote_Model_Reminder::REMINDER_STATUS_NEW) {
            $return .=  sprintf('<a href="%s">%s</a><br />',
                $this->getUrl('*/*/ignore/', array('_current' => true, 'id' => $row->getId())),
                $this->__('Ignore')
            );
        }

        if ($row->getStatus() == Mageconsult_ShopVote_Model_Reminder::REMINDER_STATUS_IGNORE) {
            $return .=  sprintf('<a href="%s">%s</a><br />',
                $this->getUrl('*/*/ignore/', array('_current' => true, 'id' => $row->getId())),
                $this->__('Dont ignore')
            );
        }

        if ($row->getStatus() !== Mageconsult_ShopVote_Model_Reminder::REMINDER_STATUS_IGNORE) {
            $return .= sprintf(' <a href="%s" onClick="confirmSetLocation(\'%s\',this.href); return false;">%s</a>',
                $this->getUrl('*/*/email/', array('_current' => true, 'reminder_id' => $row->getId())),
                $this->__('Are you sure?'),
                $this->__('Email')
            );
        }

        return $return;
    }
}
