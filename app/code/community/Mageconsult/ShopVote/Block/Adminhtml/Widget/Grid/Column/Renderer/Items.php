<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 10.09.15
 * Time: 20:46
 */

class Mageconsult_ShopVote_Block_Adminhtml_Widget_Grid_Column_Renderer_Items extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $block = $this->getLayout()->createBlock('mageconsult_shopvote_adminhtml/sales_order_item_grid');
        $block->setItemsOrdered($row->getItemsOrdered());
        return $block->toHtml();
    }
}
