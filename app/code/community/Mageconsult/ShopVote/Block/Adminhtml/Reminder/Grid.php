<?php

/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 03.09.15
 * Time: 20:50
 */
class Mageconsult_ShopVote_Block_Adminhtml_Reminder_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('grid_id');
        // $this->setDefaultSort('COLUMN_ID');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('mageconsult_shopvote/reminder')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn('reminder_id',
            array(
                'header' => $this->__('ID'),
                'index'  => 'reminder_id'
            )
        );

        $this->addColumn('ordered_at',
            array(
                'header' => $this->__('ordered at'),
                'index'  => 'ordered_at',
                'type'   => 'datetime',
            )
        );

        $this->addColumn('increment_id',
            array(
                'header' => $this->__('invoice id'),
                'index'  => 'increment_id',
                'frame_callback' => array($this, 'decorateOrderId'),
            )
        );

        $this->addColumn('email',
            array(
                'header' => $this->__('E-Mail'),
                'index'  => 'email'
            )
        );
        $this->addColumn('customer_firstname',
            array(
                'header' => $this->__('First Name'),
                'index'  => 'customer_firstname'
            )
        );

        $this->addColumn('customer_lastname',
            array(
                'header' => $this->__('Last Name'),
                'index'  => 'customer_lastname'
            )
        );

        $this->addColumn('items_ordered',
            array(
                'header'   => $this->__('ordered items'),
                'index'    => 'items_ordered',
#                'width'  => '400px',
                'renderer' => 'mageconsult_shopvote_adminhtml/widget_grid_column_renderer_items',
            )
        );

        $this->addColumn('created_at',
            array(
                'header' => $this->__('created at'),
                'index'  => 'created_at',
                'type'   => 'datetime',
            )
        );

        $this->addColumn('processed_at',
            array(
                'header' => $this->__('processed at'),
                'index'  => 'processed_at',
                'type'   => 'datetime',
            )
        );


        $this->addColumn('status',
            array(
                'header'         => $this->__('status'),
                'index'          => 'status',
                'type'           => 'options',
                'options'        => array(
                    Mageconsult_ShopVote_Model_Reminder::REMINDER_STATUS_NEW    => $this->__('new'),
                    Mageconsult_ShopVote_Model_Reminder::REMINDER_STATUS_IGNORE => $this->__('ignore'),
                    Mageconsult_ShopVote_Model_Reminder::REMINDER_STATUS_SENT   => $this->__('sent')
                ),
                'frame_callback' => array($this, 'decorateStatus'),
            )
        );

        $this->addColumn('action',
            array(
                'header'    => $this->__('Action'),
                'type'      => 'action',
                'getter'    => 'getId',
                'renderer'  => 'mageconsult_shopvote/adminhtml_widget_grid_actions',
                'filter'    => false,
                'sortable'  => false,
                'is_system' => true,
            ));


        $this->addExportType('*/*/exportCsv', $this->__('CSV'));

        $this->addExportType('*/*/exportExcel', $this->__('Excel XML'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    protected function _prepareMassaction()
    {
        $modelPk = Mage::getModel('mageconsult_shopvote/reminder')->getResource()->getIdFieldName();
        $this->setMassactionIdField($modelPk);
        $this->getMassactionBlock()->setFormFieldName('ids');
        // $this->getMassactionBlock()->setUseSelectAll(false);
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url'   => $this->getUrl('*/*/massDelete'),
        ));
        return $this;
    }

    /**
     * Decorate invoice-id column values
     *
     * @param $value
     *
     * @return string
     */
    public function decorateOrderId($field, $row)
    {
        $cell = sprintf(
            '<a href="%s"><span>%s</span></span>',
            $this->getUrl('adminhtml/sales_order/view', array('order_id' => $row->getId())),
            $field
        );
        return $cell;
    }

    /**
     * Decorate status column values
     *
     * @param $value
     *
     * @return string
     */
    public function decorateStatus($status, $row)
    {
        $severity = array(
            'critical',
            'major',
            'minor',
            'notice');

        $status = $row->getData('status');

        switch ($status) {
            case Mageconsult_ShopVote_Model_Reminder::REMINDER_STATUS_NEW:
                $class = 'critical';
                $value = 'new';
                break;
            case Mageconsult_ShopVote_Model_Reminder::REMINDER_STATUS_IGNORE:
                $class = 'minor';
                $value = 'ignore';
                break;
            case Mageconsult_ShopVote_Model_Reminder::REMINDER_STATUS_SENT:
                $class = 'notice';
                $value = 'sent';
                break;
            default:
                $class = 'critical';
                $value = 'unkown';
        }

        $cell = sprintf(
            '<span class="grid-severity-%s"><span>%s</span></span>',
            $class,
            $this->__($value)
        );
        return $cell;
    }

}
