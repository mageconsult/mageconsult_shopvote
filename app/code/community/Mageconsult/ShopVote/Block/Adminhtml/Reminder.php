<?php

/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 03.09.15
 * Time: 20:50
 */
class Mageconsult_ShopVote_Block_Adminhtml_Reminder extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_blockGroup     = 'mageconsult_shopvote';
        $this->_controller     = 'adminhtml_reminder';
        $this->_headerText     = $this->__('ShopVote Reminder List');
        $this->_addButtonLabel = $this->__('Add Email address manually');
        parent::__construct();
    }

    protected function _prepareLayout()
    {
        $this->_addButton('add_new', array(
            'label'   => Mage::helper('catalog')->__('Scan'),
            'onclick' => "setLocation('{$this->getUrl('*/*/scan')}')",
            'class'   => 'add'
        ),-10,-10);

        #$this->setChild('grid', $this->getLayout()->createBlock('adminhtml/catalog_product_grid', 'product.grid'));
        return parent::_prepareLayout();
    }

    public function getCreateUrl()
    {
        return $this->getUrl('*/*/new');
    }

}

