<?php


class Mageconsult_ShopVote_Block_Adminhtml_Sales_Order_Item_Grid extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('mageconsult_shopvote/sales/order/item/grid.phtml');
    }
}