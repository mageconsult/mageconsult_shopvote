<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 03.09.15
 * Time: 20:23
 */ 
class Mageconsult_ShopVote_Model_Resource_Reminder_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('mageconsult_shopvote/reminder');
    }

}