<?php

/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 03.09.15
 * Time: 20:23
 */
class Mageconsult_ShopVote_Model_Reminder extends Mage_Core_Model_Abstract
{
    const REMINDER_STATUS_NEW = 1;
    const REMINDER_STATUS_IGNORE = 2;
    const REMINDER_STATUS_SENT = 3;

    protected function _construct()
    {
        $this->_init('mageconsult_shopvote/reminder');
    }

    /**
     * check orders for new reminders
     */
    public function checkForNewReminders()
    {
        /** @var $salesCollection Mage_Sales_Model_Resource_Order_Collection */
        $salesCollection = Mage::getModel('sales/order')->getCollection();
        $salesCollection->addFieldToSelect('*');
        $salesCollection->addAttributeToFilter('status', 'complete');

        foreach ($salesCollection as $order) {

            /** @var $reminderModel Mageconsult_ShopVote_Model_Reminder */
            $reminderModel = Mage::getModel('mageconsult_shopvote/reminder');

            // check, if reminder already exists

            $reminderModel->load($order->getData('entity_id'), 'invoice_id');

            if (!$reminderModel->getId()) {
                $reminderModel->setData('status', self::REMINDER_STATUS_NEW);
            }

            $reminderModel->setData('email', $order->getData('customer_email'));
            $reminderModel->setData('customer_firstname', $order->getData('customer_firstname'));
            $reminderModel->setData('customer_lastname', $order->getData('customer_lastname'));
            $reminderModel->setData('invoice_id', $order->getData('entity_id'));
            $reminderModel->setData('increment_id', $order->getData('increment_id'));
            $reminderModel->setData('ordered_at', $order->getData('created_at'));
            $reminderModel->setData('created_at', now());

            $itemsOrdered = array();
            $items        = $order->getAllVisibleItems();
            foreach ($items as $item) {

                $itemsOrdered[] = array(
                    'sku'         => $item->getSku(),
                    'qty_ordered' => $item->getQtyOrdered(),
                    'name'        => $item->getName(),
                );
            }
            $reminderModel->setData('items_ordered', serialize($itemsOrdered));

            // save model
            try {
                $reminderModel->save();
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(Mage::helper('mageconsult_shopvote')->__('Unable to save the Shopvote Reminders.'));
                Mage::logException($e);
            }
        }
    }


    /**
     * Queue email with new order data
     *
     * @param bool $forceMode if true then email will be sent regardless of the fact that it was already sent previously
     *
     * @return Mage_Sales_Model_Order
     * @throws Exception
     */
    public function queueNewReminderEmail($forceMode = false)
    {
        $storeId = $this->getStore()->getId();

        // Get the destination email addresses to send copies to
        $copyTo     = $this->_getEmails(self::XML_PATH_EMAIL_COPY_TO);
        $copyMethod = Mage::getStoreConfig(self::XML_PATH_EMAIL_COPY_METHOD, $storeId);

        // Start store emulation process
        /** @var $appEmulation Mage_Core_Model_App_Emulation */
        $appEmulation           = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

        try {
            // Retrieve specified view block from appropriate design package (depends on emulated store)
            $paymentBlock = Mage::helper('payment')->getInfoBlock($this->getPayment())
                ->setIsSecureMode(true);
            $paymentBlock->getMethod()->setStore($storeId);
            $paymentBlockHtml = $paymentBlock->toHtml();
        } catch (Exception $exception) {
            // Stop store emulation process
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            throw $exception;
        }

        // Stop store emulation process
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        // Retrieve corresponding email template id and customer name
        if ($this->getCustomerIsGuest()) {
            $templateId   = Mage::getStoreConfig(self::XML_PATH_EMAIL_GUEST_TEMPLATE, $storeId);
            $customerName = $this->getBillingAddress()->getName();
        } else {
            $templateId   = Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE, $storeId);
            $customerName = $this->getCustomerName();
        }

        /** @var $mailer Mage_Core_Model_Email_Template_Mailer */
        $mailer = Mage::getModel('core/email_template_mailer');
        /** @var $emailInfo Mage_Core_Model_Email_Info */
        $emailInfo = Mage::getModel('core/email_info');
        $emailInfo->addTo($this->getCustomerEmail(), $customerName);
        if ($copyTo && $copyMethod == 'bcc') {
            // Add bcc to customer email
            foreach ($copyTo as $email) {
                $emailInfo->addBcc($email);
            }
        }
        $mailer->addEmailInfo($emailInfo);

        // Email copies are sent as separated emails if their copy method is 'copy'
        if ($copyTo && $copyMethod == 'copy') {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);
        $mailer->setTemplateParams(array(
            'order'        => $this,
            'billing'      => $this->getBillingAddress(),
            'payment_html' => $paymentBlockHtml
        ));

        /** @var $emailQueue Mage_Core_Model_Email_Queue */
        $emailQueue = Mage::getModel('core/email_queue');
        $emailQueue->setEntityId($this->getId())
            ->setEntityType(self::ENTITY)
            ->setEventType(self::EMAIL_EVENT_NAME_NEW_ORDER)
            ->setIsForceCheck(!$forceMode);

        $mailer->setQueue($emailQueue)->send();

        $this->setEmailSent(true);
        $this->_getResource()->saveAttribute($this, 'email_sent');

        return $this;
    }

    /**
     * Send email with order data
     *
     * @return Mage_Sales_Model_Order
     */
    public function sendNewReminderEmail()
    {
        $this->queueNewReminderEmail(true);
        return $this;
    }

}